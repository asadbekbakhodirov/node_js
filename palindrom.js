
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Enter the word : ', (a) => {
    function reverseString(str) {
        return str.split("").reverse().join("");
    }
    if (reverseString(a) === a){
        console.log("This is palendrom word")
    } else {
        console.log("This is not palendrom word")
    }
  rl.close();
});